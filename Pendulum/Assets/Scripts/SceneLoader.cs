﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// ゲームシーン
/// </summary>
public enum SCENE
{
    SCENEBASE,
    GAME,
    GAMEUI,
    DEBUG,
    MAX,
};
/// <summary>
/// シーン遷移直前に開いていたシーンを覚えておくためのクラス
/// </summary>
public class OldSceneData
{
    public static readonly OldSceneData instance = new OldSceneData();
    public string referer = string.Empty;
}

/// <summary>
/// シーン読み込み器.
/// </summary>
public class SceneLoader : MonoBehaviour
{
    WaitForEndOfFrame waitforEndOfFrame = new WaitForEndOfFrame();
    /// <summary>
    /// シーン名リスト
    /// </summary>
    static readonly string[] SceneName = new string[(int)SCENE.MAX]
    {
        "SceneBase",
        "Game",
        "GameUI",
        "Debug",
    };

    /// <summary>
    /// ロード画面UI
    /// </summary>
    [SerializeField]
    GameObject SceneLoadingUI = null;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start()
    {
        if (SceneLoadingUI != null)
        {
            InvisibleNowLoading();
        }
    }

    /// <summary>
    /// ロード画面表示
    /// </summary>
    public void VisibleNowLoading()
    {
        if (SceneLoadingUI != null)
        {
            SceneLoadingUI.SetActive(true);
        }
    }
    /// <summary>
    /// ロード画面非表示処理
    /// </summary>
    public void InvisibleNowLoading()
    {
        if (SceneLoadingUI != null)
        {
            SceneLoadingUI.SetActive(false);
        }
    }

    #region 単体シーンロード
    AsyncOperation async = null;

    /// <summary>
    /// シーン遷移前ロード
    /// </summary>
    /// <param name="gameScene"></param>
    /// <param name="withLaunch"></param>
    /// <returns></returns>
    IEnumerator ScenePreload(SCENE scene, bool withLaunch)
    {
        if (async == null)
        {
            launchPermitte = false;
            async = SceneManager.LoadSceneAsync(SceneName[(int)scene], LoadSceneMode.Single);
            async.allowSceneActivation = false;    // シーン遷移をしない

            while (async.progress < 0.9f)
            {
                yield return waitforEndOfFrame;
            }
            yield return waitforEndOfFrame;

            launchPermitte = true;
            if (withLaunch)
            {
                LaunchSceneSingle();
            }
        }
    }
    /// <summary>
    /// シーンプレロード
    /// GetLaunchPermitte() でロード状況を確認し
    /// LaunchSceneSingle() を呼び出してください
    /// </summary>
    /// <param name="scene"></param>
    public void Preload(SCENE scene)
    {
        StartCoroutine(ScenePreload(scene, false));
    }
    /// <summary>
    /// ロード完了時に自動でシーンを呼び出す
    /// </summary>
    /// <param name="scene"></param>
    public void LoadAndLaunch(SCENE scene)
    {
        StartCoroutine(ScenePreload(scene, true));
    }
    /// <summary>
    /// シーン遷移実行
    /// </summary>
    public void LaunchSceneSingle()
    {
        if (async == null)
        {
            Debug.LogWarning("LaunchSceneSingle():遷移先が指定されていない");
        }
        else
        {
            if (!launchPermitte)
            {
                Debug.LogWarning("LaunchSceneSingle():プリロードが完了していない");
            }
            else
            {
                Debug.Log("LaunchSceneSingle():Launch");
                OldSceneData.instance.referer = SceneManager.GetActiveScene().name;
                async.allowSceneActivation = true;
                async = null;
                InvisibleNowLoading();
            }
        }
    }

    /// <summary>
    /// 遷移許可フラグ
    /// </summary>
    bool launchPermitte = false;
    public bool GetLaunchPermitte()
    {
        return launchPermitte;
    }
    #endregion

    #region 複数シーンロード
    List<AsyncOperation> multiAsync = null;
    IEnumerator ScenePreload(List<SCENE> scene, bool withLaunch)
    {
        if (multiAsync == null)
        {
            multiAsync = new List<AsyncOperation>();
            launchPermitteAdd = false;
            for (int i = 0;i < scene.Count; i++)
            {
                multiAsync.Add(SceneManager.LoadSceneAsync(SceneName[(int)scene[i]], LoadSceneMode.Additive));
                multiAsync[i].allowSceneActivation = false;    // シーン遷移をしない
            }

            bool allAsyncProgressOverThreshold = false;
            while (!allAsyncProgressOverThreshold)
            {
                allAsyncProgressOverThreshold = true;
                for (int i = 0;i < multiAsync.Count; i++)
                {
                    if(multiAsync[i].progress < 0.9f)
                    {
                        allAsyncProgressOverThreshold = false;
                    }
                }
                yield return waitforEndOfFrame;
            }
            yield return waitforEndOfFrame;

            launchPermitteAdd = true;
            if (withLaunch)
            {
                LaunchSceneAdditive();
            }
        }
    }
    /// <summary>
    /// シーンプレロード
    /// GetLaunchPermitteAdd() でロード状況を確認し
    /// LaunchSceneAdditive() を呼び出してください
    /// </summary>
    /// <param name="scene"></param>
    public void Preload(List<SCENE> scene)
    {
        StartCoroutine(ScenePreload(scene, false));
    }
    /// <summary>
    /// ロード完了時に自動でシーンを呼び出す
    /// </summary>
    /// <param name="scene"></param>
    public void LoadAndLaunch(List<SCENE> scene)
    {
        StartCoroutine(ScenePreload(scene, true));
    }
    /// <summary>
    /// シーン遷移実行
    /// </summary>
    public void LaunchSceneAdditive()
    {
        if (multiAsync.Count == 0)
        {
            Debug.LogWarning("LaunchSceneAdditive():遷移先が指定されていない");
        }
        else
        {
            if (!launchPermitteAdd)
            {
                Debug.LogWarning("LaunchSceneAdditive():プリロードが完了していない");
            }
            else
            {
                Debug.Log("LaunchSceneAdditive():Launch");
                OldSceneData.instance.referer = SceneManager.GetActiveScene().name;
                for (int i = 0; i < multiAsync.Count; i++)
                {
                    multiAsync[i].allowSceneActivation = true;
                }
                InvisibleNowLoading();
            }
        }
    }

    bool launchPermitteAdd = false;
    public bool GetLaunchPermitteAdd()
    {
        return launchPermitteAdd;
    }
    #endregion

    /// <summary>
    /// 追加シーンを破棄
    /// </summary>
    /// <param name="scene"></param>
    public void SceneUnload(SCENE scene)
    {
        StartCoroutine(Unload(scene));
    }
    AsyncOperation unloadAsync = null;
    IEnumerator Unload(SCENE scene)
    {
        if (unloadAsync == null)
        {
            unloadAsync = SceneManager.UnloadSceneAsync(SceneName[(int)scene]);
            unloadAsync.allowSceneActivation = false;

            while (unloadAsync.progress < 0.9f)
            {
                yield return waitforEndOfFrame;
            }
            yield return waitforEndOfFrame;

            unloadAsync.allowSceneActivation = true;
            unloadAsync = null;
        }
    }
}
