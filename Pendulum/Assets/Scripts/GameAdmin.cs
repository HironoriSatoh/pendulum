﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームの状態
/// </summary>
public enum GAME_STATE
{
    WAIT,
    PLAY,
    DEAD,
    CLEAR,
    MAX
}

public class GameAdmin : MonoBehaviour
{
    /// <summary>
    /// 障害物とか
    /// </summary>
    [SerializeField]
    Transform world = null;

    /// <summary>
    /// 振り子処理
    /// </summary>
    [SerializeField]
    PendulumController pendulumController = null;

    /// <summary>
    /// 振り子球
    /// </summary>
    [SerializeField]
    PendulumBall pendulumBall = null;

    GAME_STATE state = GAME_STATE.WAIT;
    public void SetGameState (GAME_STATE set)
    {
        state = set;
    }
    public GAME_STATE GetGameState()
    {
        return state;
    }

    private void Start()
    {
        ResetGame();
    }

    public void ResetGame()
    {
        SetGameState(GAME_STATE.WAIT);
        pendulumController.ResetState();
        pendulumBall.ResetState();
        foreach (Transform child in world.transform)
        {
            child.gameObject.SetActive(true);
        }

        RemovableObstacle[] ro = world.GetComponentsInChildren<RemovableObstacle>();
        for (int i = 0; i < ro.Length; i++)
        {
            ro[i].ResetState();
        }
    }
}
