﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiSceneManager : MonoBehaviour {
    SceneLoader sceneLoader = null;
	// Use this for initialization
	void Awake ()
    {
        sceneLoader = GameObject.Find("Loader").GetComponent<SceneLoader>();
        sceneLoader.VisibleNowLoading();
        sceneLoader.LoadAndLaunch(new List<SCENE> { SCENE.GAME, SCENE.GAMEUI, SCENE.DEBUG });
	}
	
}
