﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumController : MonoBehaviour
{
    [SerializeField]
    GameAdmin admin = null;

    /// <summary>
    /// ゴール時の挙動
    /// ※Awakeで設定して!
    /// </summary>
    public Action goal = null;
    /// <summary>
    /// 死亡時の挙動
    /// ※Awakeで設定して!
    /// </summary>
    public Action dead = null;

    /// <summary>
    /// スプリングの最長距離
    /// </summary>
    const float SPRING_MAX_DISTANCE = 40.0f;

    Camera cam = null;

    /// <summary>
    /// 振り子軸
    /// </summary>
    [SerializeField]
    Transform parentSphere = null;
    /// <summary>
    /// 振り子の子
    /// </summary>
    [SerializeField]
    Transform childSphere = null;

    /// <summary>
    /// 軸と子を結ぶ線
    /// </summary>
    [SerializeField]
    PendulumLine pendulumLine = null;
    /// <summary>
    /// ボール
    /// </summary>
    [SerializeField]
    PendulumBall pendulumBall = null;

    /// <summary>
    /// ヒンジ
    /// </summary>
    HingeJoint childHinge = null;
    /// <summary>
    /// hingeJoint に設定する親のrigidbody
    /// </summary>
    [SerializeField]
    Rigidbody parentRig = null;
    /// <summary>
    /// 子のrigidbody
    /// </summary>
    [SerializeField]
    Rigidbody childRig = null;

    [SerializeField]
    Transform start = null;

    private void Awake()
    {
        cam = Camera.main;
    }

    void Start()
    {
        pendulumBall.dead = () =>
        {
            if(dead!= null)
            {
                dead();
            }
        };
        pendulumBall.goal = () =>
        {
            if (goal != null)
            {
                goal();
            }
        };
        pendulumBall.hitWall = (Collision col) =>
        {
            MouseInputEnd();
            childRig.velocity = col.relativeVelocity;
        };
    }

    public void ResetState()
    {
        DeleteHinge();

        parentSphere.position = start.position;
        childSphere.position = start.position;

        childRig.isKinematic = true;

        childRig.velocity = Vector3.zero;
        childRig.angularVelocity = Vector3.zero;

        childSphere.eulerAngles = Vector3.zero;
        childSphere.Translate(new Vector3(0.0f, -15.0f, 0.0f));

        pendulumLine.ShowLine();
    }

    // Update is called once per frame
    void Update()
    {
        switch (admin.GetGameState())
        {
            case GAME_STATE.WAIT:
                if (Input.GetMouseButtonDown(0))
                {
                    admin.SetGameState(GAME_STATE.PLAY);
                    //1タップ毎に子を1破棄する
                    MouseInputEvent();
                }
                pendulumLine.LineUpdate(parentSphere, childSphere);
                cam.transform.position = new Vector3(childSphere.position.x, childSphere.position.y, -40.0f);

                break;
            case GAME_STATE.PLAY:
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        MouseInputEvent();
                    }
                    else if (Input.GetMouseButton(0))
                    {
                        MouseInputKeep();
                    }
                    else if (Input.GetMouseButtonUp(0))
                    {
                        MouseInputEnd();
                    }

                    pendulumLine.LineUpdate(parentSphere, childSphere);
                    cam.transform.position = new Vector3(childSphere.position.x, childSphere.position.y, -40.0f);
                }
                break;
            case GAME_STATE.DEAD:
            case GAME_STATE.CLEAR:
                DeleteHinge();
                break;
            default:
                Debug.LogError("未登録の GAME_STATE がある");
                break;
        }
    }

    /// <summary>
    /// 振り子に謎パワーを与えるフラグ
    /// </summary>
    bool ForceAdd = false;

    /// <summary>
    /// マウスボタン押下
    /// </summary>
    void MouseInputEvent()
    {
        ForceAdd = true;
        pendulumBall.RemoveChild(1);

        Vector3 velocity = childRig.velocity;
        childRig.isKinematic = true;
        childSphere.transform.eulerAngles = Vector3.zero;

        Vector2 mouseInputFirstPos = Input.mousePosition;
        parentSphere.position = cam.ScreenToWorldPoint(new Vector3(mouseInputFirstPos.x, mouseInputFirstPos.y, 40.0f));
        SetHinge(Vector3.Distance(childSphere.position, parentSphere.position) / 2.0f);

        childRig.isKinematic = false;
        childRig.velocity = velocity;

        DisableGraivty();
    }

    /// <summary>
    /// マウスボタン押下継続
    /// </summary>
    void MouseInputKeep()
    {
        if (ForceAdd)
        {
            Vector3 f = 10 * -(Vector3.Normalize(parentSphere.position - childSphere.position));
            f.y *= -1;
            float fx = f.x;
            f.x = f.y;
            f.y = fx;
            childRig.AddForce(f);
        }
    }

    /// <summary>
    /// マウスボタン押下終了
    /// </summary>
    void MouseInputEnd()
    {
        ForceAdd = false;
        enableGravity();
    }

    /// <summary>
    /// ヒンジのコンポーネントを設定
    /// </summary>
    /// <param name="distance"></param>
    void SetHinge(float distance)
    {
        DeleteHinge();

        childHinge = childRig.gameObject.AddComponent<HingeJoint>();
        childHinge.axis = new Vector3(0.0f, 0.0f, 1.0f);
        childHinge.anchor = (parentSphere.position - childSphere.position) / 2;// new Vector3(0.0f, distance, 0.0f);
        childHinge.autoConfigureConnectedAnchor = false;
        childHinge.connectedBody = parentRig;
        childHinge.connectedAnchor = Vector3.zero;

        pendulumLine.ShowLine();
    }

    void DeleteHinge()
    {
        if (childHinge != null)
        {
            Destroy(childHinge);
            childHinge = null;
        }
        pendulumLine.HideLIne();
    }

    void enableGravity()
    {
        childRig.useGravity = true;
    }
    void DisableGraivty()
    {
        childRig.useGravity = false;
    }
}
