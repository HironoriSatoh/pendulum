﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumLine : MonoBehaviour
{
    [SerializeField]
    LineRenderer lineRenderer = null;

    Coroutine draw = null;

    public void DrawLine(Vector3 from, Vector3 to, float animationTime)
    {
        skip = false;
        draw = StartCoroutine(Draw(from, to, animationTime));
    }

    public void DrawStop()
    {
        skip = true;
    }

    bool skip = false;
    IEnumerator Draw(Vector3 from,Vector3 to, float limit)
    {
        float progress = 0.0f;
        float time = 0.0f;

        while (progress < 1.0f && !skip)
        {
            time += Time.deltaTime;
            progress = time / limit;

            Vector3 pos = Vector3.Slerp(from, to, progress);
            lineRenderer.SetPosition(2, pos);
            yield return null;
        }

        lineRenderer.SetPosition(2, to);
        draw = null;
    }

    public void LineUpdate(Transform parentSphere,Transform childSphere)
    {
        lineRenderer.SetPosition(0, parentSphere.position);
        lineRenderer.SetPosition(1, childSphere.position);
        lineRenderer.SetPosition(2, childSphere.position);
    }

    public void ShowLine()
    {
        lineRenderer.enabled = true;
    }

    public void HideLIne()
    {
        lineRenderer.enabled = false;
    }
}
