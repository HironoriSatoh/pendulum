﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumBall : MonoBehaviour
{
    public Action<Collision> hitWall = null;
    public Action goal = null;
    public Action dead = null;
    [SerializeField]
    GameAdmin admin = null;

    [SerializeField]
    Rigidbody ballRig = null;

    bool Alive = true;
    bool Cleared = false;

    List<GameObject> childs = new List<GameObject>();
    List<Vector3> oldPos = new List<Vector3>();

    [SerializeField]
    GameObject ball = null;
    [SerializeField]
    Transform sphereBase = null;

    [SerializeField]
    TextMesh textMesh = null;

    private void FixedUpdate()
    {
        if (childs.Count > 0)
        {
            for (int i = childs.Count - 1; i > 0; i--)
            {
                oldPos[i] = oldPos[i - 1];
            }
            oldPos[0] = this.transform.position;
        }
    }

    private void Update()
    {
        textMesh.transform.position = this.transform.position;
        for (int i = childs.Count - 1; i >= 0; i--)
        {
            childs[i].transform.position = oldPos[i];
        }
    }

    public void ResetState()
    {
        Alive = true;
        Cleared = false;

        childs = new List<GameObject>();
        oldPos = new List<Vector3>();

        foreach (Transform child in sphereBase)
        {
            Destroy(child.gameObject);
        }

        AddChild(11);

        textMesh.text = childs.Count.ToString();
    }

    public bool IsAlive()
    {
        return Alive;
    }

    void Die()
    {
        RemoveAllChild();
        childs = new List<GameObject>();
        oldPos = new List<Vector3>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Killer":
                {
                    if (!Cleared)
                    {
                        Die();

                        admin.SetGameState(GAME_STATE.DEAD);
                        Alive = false;
                        if (dead != null)
                        {
                            dead();
                        }
                        textMesh.text = childs.Count.ToString();
                    }
                }
                break;
            case "UnhookableObstacle":
                {
                    if (Alive)
                    {
                        if (hitWall != null)
                        {
                            hitWall(collision);
                        }
                    }
                }
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Goal":
                {
                    if (Alive)
                    {
                        admin.SetGameState(GAME_STATE.CLEAR);
                        Cleared = true;
                        if (goal != null)
                        {
                            goal();
                        }
                    }
                }
                break;
            case "AddSphere":
                {
                    if (Alive)
                    {
                        if (!Cleared)
                        {
                            var aci = other.GetComponent<AddChildInfo>();
                            other.gameObject.SetActive(false);
                            if (aci != null)
                            {
                                AddChild(aci.addCount);
                            }
                        }
                    }
                }
                break;
            case "Removable":
                {
                    if (Alive)
                    {
                        if (!Cleared)
                        {
                            RemovableObstacle ro = other.transform.GetComponent<RemovableObstacle>();
                            int nc = ro.UpdateNeedCount(childs.Count);
                            RemoveChild(nc);
                        }
                    }
                }
                break;
        }
    }

    /// <summary>
    /// 追従する球を全削除
    /// </summary>
    public void RemoveAllChild()
    {
        int cc = childs.Count;
        for (int i = 0; i < cc; i++)
        {
            childs[i].AddComponent<Rigidbody>();
            childs[i].AddComponent<SphereCollider>();
        }
    }

    /// <summary>
    /// 追従する球を任意の数破棄
    /// </summary>
    /// <param name="count"></param>
    public void RemoveChild(int count)
    {
        if (count <= childs.Count)
        {
            for (int i = 0; i < count; i++)
            {
                childs[0].AddComponent<Rigidbody>();
                childs[0].AddComponent<SphereCollider>();

                childs.RemoveAt(0);
                oldPos.RemoveAt(oldPos.Count - 1);
            }
        }
        else
        {
            Die();

            admin.SetGameState(GAME_STATE.DEAD);
            Alive = false;
            if (dead != null)
            {
                dead();
            }
        }

        textMesh.text = childs.Count.ToString();
    }

    /// <summary>
    /// 追従する球を任意の数追加
    /// </summary>
    /// <param name="count"></param>
    void AddChild(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var obj = Instantiate(ball, sphereBase);
            childs.Add(obj);
            Vector3 newPos = this.transform.position;

            if (oldPos.Count != 0)
            {
                newPos = oldPos[oldPos.Count - 1];
            }
            oldPos.Add(newPos);
        }
        textMesh.text = childs.Count.ToString();
    }
}
