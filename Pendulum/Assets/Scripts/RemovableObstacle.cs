﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemovableObstacle : MonoBehaviour {

    [SerializeField]
    int needCount = 10;
    int need = 0;
    [SerializeField]
    TextMesh textMesh = null;

    private void Awake()
    {
        need = needCount;
        textMesh.text = "need " + need;
        
    }

    public void ResetState()
    {
        need = needCount;
        textMesh.text = "need " + need;
    }

    public int UpdateNeedCount(int count)
    {
        int ret = need;
        need -= count;
        textMesh.text = "need " + need;
        if(need <= 0)
        {
            this.gameObject.SetActive(false);
        }

        return ret;
    }
}
