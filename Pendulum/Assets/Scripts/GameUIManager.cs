﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{
    GameAdmin gameAdmin = null;
    PendulumController pendulumController = null;

    [SerializeField]
    GameObject clearedUI = null;
    [SerializeField]
    GameObject deadUI = null;

    [SerializeField]
    Button GoaledResetButton = null;
    [SerializeField]
    Button DeadResetButton = null;

    // Use this for initialization
    void Awake () {
        
        pendulumController = GameObject.Find("Player").GetComponent<PendulumController>();

        pendulumController.dead = () =>
        {
            deadUI.SetActive(true);
        };
        pendulumController.goal = () =>
        {
            clearedUI.SetActive(true);
        };


        gameAdmin = GameObject.Find("GameAdmin").GetComponent<GameAdmin>();
        GoaledResetButton.onClick.AddListener(() =>
        {
            HideUI();
            gameAdmin.ResetGame();
        });
        DeadResetButton.onClick.AddListener(() =>
        {
            HideUI();
            gameAdmin.ResetGame();
        });
    }

    public void HideUI()
    {
        clearedUI.SetActive(false);
        deadUI.SetActive(false);
    }
}
