﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// デバッグ機能の初期化をこの中に
/// </summary>
public class Debugger : MonoBehaviour
{
    GameAdmin gameAdmin = null;
    GameUIManager gameUIManager = null;

    [SerializeField]
    Button resetButton = null;



    // Use this for initialization
    void Awake()
    {
        gameAdmin = GameObject.Find("GameAdmin").GetComponent<GameAdmin>();
        gameUIManager = GameObject.Find("GameUICanvas").GetComponent<GameUIManager>();

        resetButton.onClick.AddListener(() =>
        {
            gameUIManager.HideUI();
            gameAdmin.ResetGame();
        });
    }
}
