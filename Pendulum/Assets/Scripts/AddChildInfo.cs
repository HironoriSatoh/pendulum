﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddChildInfo : MonoBehaviour
{
    public int addCount = 1;

    [SerializeField]
    TextMesh textMesh = null;

    private void Start()
    {
        textMesh.text = "+" + addCount;
    }
}
