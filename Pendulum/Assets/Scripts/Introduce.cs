﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Introduce : MonoBehaviour
{
    //@note表示したい順序でインスペクターから Image を追加してください
    [SerializeField]
    Image[] logo = null;

    Coroutine animation = null;

    [SerializeField]
    SceneLoader sceneLoader = null;

    // Use this for initialization
    void Start()
    {
        //ロゴ表示開始
        animation = StartCoroutine(IntroduceImageAnimation(logo));

        sceneLoader.Preload (SCENE.SCENEBASE);
    }

    /// <summary>
    /// 配列内のすべてのイメージをフェードイン/フェードアウトで表示し画面遷移を行う
    /// </summary>
    /// <param name="img"></param>
    /// <returns></returns>
    IEnumerator IntroduceImageAnimation(Image[] img)
    {
        const float FadingTime = 0.25f;
        const float ShowTime = 1.5f;
        float currentTime = 0.0f;
        float progress = 0.0f;

        for (int i = 0; i < img.Length; i++)
        {
            //フェードイン
            currentTime = 0.0f;
            progress = 0.0f;
            while (progress <= 1.0f)
            {
                img[i].color = new Color(img[i].color.r, img[i].color.g, img[i].color.b, (progress));
                WaitSecProgress(ref progress, ref currentTime, FadingTime);
                SkipShowLogo(ref progress, ref currentTime, FadingTime);
                yield return null;
            }
            img[i].color = new Color(img[i].color.r, img[i].color.g, img[i].color.b, (1.0f));
            //しばらく見せる
            currentTime = 0.0f;
            progress = 0.0f;
            while (progress <= 1.0f)
            {
                WaitSecProgress(ref progress, ref currentTime, ShowTime);
                SkipShowLogo(ref progress, ref currentTime, ShowTime);
                yield return null;
            }
            //フェードアウト
            currentTime = 0.0f;
            progress = 0.0f;
            while (progress <= 1.0f)
            {
                img[i].color = new Color(img[i].color.r, img[i].color.g, img[i].color.b, (1.0f - progress));
                WaitSecProgress(ref progress, ref currentTime, FadingTime);
                SkipShowLogo(ref progress, ref currentTime, FadingTime);
                yield return null;
            }
            img[i].color = new Color(img[i].color.r, img[i].color.g, img[i].color.b, (0.0f));
        }
        
        yield return new WaitWhile(() => !sceneLoader.GetLaunchPermitte());
        sceneLoader.LaunchSceneSingle();

        Destroy(gameObject);
    }

    /// <summary>
    /// 時間待ち共通処理
    /// </summary>
    /// <param name="_time"></param>
    /// <param name="_limit"></param>
    /// <returns></returns>
    void WaitSecProgress(ref float _progress, ref float _time, float _limit)
    {
        _time += Time.deltaTime;
        _progress = _time / _limit;
    }

    /// <summary>
    /// ロゴ表示スキップ
    /// </summary>
    /// <param name="_progress"></param>
    /// <param name="_time"></param>
    /// <param name="limit"></param>
    void SkipShowLogo(ref float _progress, ref float _time, float limit)
    {
        if (Input.GetMouseButtonDown(0))
        {
            _time = limit;
            _progress = 1.0f;
        }
    }
}